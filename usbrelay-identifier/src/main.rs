use libusbrelay::usbrelay::UsbRelay;
use std::env;
use std::io::{Error, ErrorKind};
fn main() -> std::io::Result<()> {
    let relay = UsbRelay::from_path(env::args().nth(1).ok_or(Error::new(
        ErrorKind::Other,
        "Expect path to a hid device of type usbrelay",
    ))?)?;
    print!("{}", relay.info()?.serial);
    Ok(())
}
