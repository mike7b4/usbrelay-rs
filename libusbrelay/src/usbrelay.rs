use crate::hid::Hid;
use log::error;
use std::fmt;
use std::io::Write;
use std::io::{Error, ErrorKind, Result};
pub struct UsbRelayInfo {
    pub serial: String,
    pub state: u8,
}

impl fmt::Display for UsbRelayInfo {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        writeln!(f, "Serial: {}", self.serial)?;
        writeln!(f, "Relay states: {:08b}", self.state)
    }
}

impl From<&[u8; 8]> for UsbRelayInfo {
    fn from(bytes: &[u8; 8]) -> Self {
        Self {
            serial: String::from_utf8_lossy(&bytes[0..5]).to_string(),
            state: bytes[7],
        }
    }
}

pub struct UsbRelay {
    hid: Hid,
    num_relays: u8,
}

impl UsbRelay {
    pub fn from_path<P: AsRef<std::path::Path>>(p: P) -> Result<Self> {
        Ok(Self {
            hid: Hid::from_path(p)?,
            num_relays: 2, // TODO,
        })
    }

    /// Get serial and current state of all relays
    pub fn info(&self) -> Result<UsbRelayInfo> {
        let buf: [u8; 8] = [1, 0, 0, 0, 0, 0, 0, 0];
        let len = self.hid.get_feature_report(&buf)?;
        if len != 8 {
            return Err(Error::from(ErrorKind::InvalidData));
        }
        Ok(UsbRelayInfo::from(&buf))
    }

    /// relay_id is the index from 1 to number of relays
    pub fn set(&mut self, relay_id: u8, state: bool) -> Result<()> {
        if relay_id == 0 || relay_id > self.num_relays {
            error!("relay_id must be between 1 and {}", self.num_relays);
            return Err(Error::from(ErrorKind::InvalidInput));
        }
        let buf: [u8; 9] = [
            0,
            if state { 0xFF } else { 0xFD },
            relay_id,
            0,
            0,
            0,
            0,
            0,
            0,
        ];
        self.hid.write_all(&buf)?;
        Ok(())
    }

    /// Get relay state
    pub fn state(&self, relay_id: u8) -> Result<bool> {
        if relay_id == 0 || relay_id > self.num_relays {
            error!("relay_id must be between 1 and {}", self.num_relays);
            return Err(Error::from(ErrorKind::InvalidInput));
        }
        let info = self.info()?;
        Ok((info.state & (1 << (relay_id - 1))) != 0)
    }
}
